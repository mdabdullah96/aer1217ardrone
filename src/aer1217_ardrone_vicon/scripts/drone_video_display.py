#!/usr/bin/env python

# A basic video display window for the tutorial "Up and flying with the AR.Drone and ROS | Getting Started"
# https://github.com/mikehamer/ardrone_tutorials_getting_started

# This display window listens to the drone's video feeds and updates the display at regular intervals
# It also tracks the drone's status and any connection problems, displaying them in the window's status bar
# By default it includes no control functionality. The class can be extended to implement key or mouse listeners if required

# Import the ROS libraries, and load the manifest file which through <depend package=... /> will give us access to the project dependencies
import roslib
import rospy
import cv2

# Import the two types of messages we're interested in
from sensor_msgs.msg import Image    	 # for receiving the video feed
from ardrone_autonomy.msg import Navdata # for receiving navdata feedback
from std_msgs.msg import Int32
from ardrone_autonomy.srv import *

# We need to use resource locking to handle synchronization between GUI thread and ROS topic callbacks
from threading import Lock

# An enumeration of Drone Statuses
from drone_status import DroneStatus

# The GUI libraries
from PySide import QtCore, QtGui

# 2017-03-22 Import libraries from OpenCV
# OpenCV Bridge http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython
from cv_bridge import CvBridge, CvBridgeError
import numpy as np

from std_msgs.msg import Empty, Int8, String
from geometry_msgs.msg import Point, PoseStamped
from tf.transformations import euler_from_quaternion, quaternion_matrix
from geometry_msgs.msg import TransformStamped, Twist
import time
import os
from datetime import datetime


from hoop_detect import hoopDetect
from hazard_detect import hazardDetect


# Some Constants
CONNECTION_CHECK_PERIOD = 250 #ms
GUI_UPDATE_PERIOD = 20 #ms
DETECT_RADIUS = 4 # the radius of the circle drawn when a tag is detected

# time in centi-seconds
def get_time():
	return (100*time.time())

class DroneVideoDisplay(QtGui.QMainWindow):
	StatusMessages = {
		DroneStatus.Emergency : 'Emergency',
		DroneStatus.Inited    : 'Initialized',
		DroneStatus.Landed    : 'Landed',
		DroneStatus.Flying    : 'Flying',
		DroneStatus.Hovering  : 'Hovering',
		DroneStatus.Test      : 'Test (?)',
		DroneStatus.TakingOff : 'Taking Off',
		DroneStatus.GotoHover : 'Going to Hover Mode',
		DroneStatus.Landing   : 'Landing',
		DroneStatus.Looping   : 'Looping (?)'
		}
	DisconnectedMessage = 'Disconnected'
	UnknownMessage = 'Unknown Status'
	
	def __init__(self):
		# Construct the parent class
		super(DroneVideoDisplay, self).__init__()

		# Setup our very basic GUI - a label which fills the whole window and holds our image
		self.setWindowTitle('AR.Drone Video Feed')
		self.imageBox = QtGui.QLabel(self)
		self.setCentralWidget(self.imageBox)

		# Subscribe to the /ardrone/navdata topic, of message type navdata, and call self.ReceiveNavdata when a message is received
		self.subNavdata = rospy.Subscriber('/ardrone/navdata',Navdata,self.ReceiveNavdata,queue_size=1) 

		# subscribing to the Vicon topic so that the actual positon of the drone can be 
		# saved at the same timesteps as the images
		self.vicon_topic = '/vicon/ARDroneCarre/ARDroneCarre'
		self._vicon_msg = TransformStamped()
		rospy.Subscriber(self.vicon_topic, TransformStamped, self._vicon_callback)
		
		# Subscribe to the drone's video feed, calling self.ReceiveImage when a new frame is received
		self.subVideo   = rospy.Subscriber('/ardrone/image_raw',Image,self.ReceiveImage,queue_size=1)
		
		# Subscribe to image_process to know when to process images
		rospy.Subscriber('/image_process', Int8, self.image_flag)
		self.image_hoop = False #boolean to start image process for hoop
		self.image_hazard = False #boolean to start image process for hazard
		
		# Publish to hazard topic
		self.pub_hazard_sign = rospy.Publisher("/hazard", String, queue_size = 10) #publisher for hazard manuever

		self.pub_keyboard_alive = rospy.Publisher('/ardrone/keyboard_alive', Int32, queue_size=1)  # publish alive message
		
		# Holds the image frame received from the drone and later processed by the GUI
		self.image = None
		self.imageLock = Lock()

		self.tags = []
		self.tagLock = Lock()
				
		# Holds the status message to be displayed on the next GUI update
		self.statusMessage = ''

		# Tracks whether we have received data since the last connection check
		# This works because data comes in at 50Hz but we're checking for a connection at 4Hz
		self.communicationSinceTimer = False
		self.connected = False

		# A timer to check whether we're still connected
		self.connectionTimer = QtCore.QTimer(self)
		self.connectionTimer.timeout.connect(self.ConnectionCallback)
		self.connectionTimer.start(CONNECTION_CHECK_PERIOD)
		
		# A timer to redraw the GUI
		self.redrawTimer = QtCore.QTimer(self)
		self.redrawTimer.timeout.connect(self.RedrawCallback)
		self.redrawTimer.start(GUI_UPDATE_PERIOD)
		
		# 2017-03-22 convert ROS images to OpenCV images
		self.bridge = CvBridge()

		# 2017-03-31 Lab 4 processing images variables
		self.processImages = False		
		self.cv_output = None
		self.cv_img = None

		# save image every one second. given the trajectory used and the slow speed
		# saving 1 image per second is sufficient. more than this might introduce latency
		# image recording starts automatically but the button 'x' must be pressed to switch to the bottom facing
		# camera
		# this is done instead of using a loop since its the only way to ensure that there 
		# is only instance of this class instantiated
		self.commandTimer = rospy.Timer(rospy.Duration(.05),self.SaveImage)


		# color boundaries, in the form of (lower[B,G,R], upper[B,G,R]) 
		# TODO try and test out the color BGR values
		self.HSV_low = [80,10,120]
		self.HSV_high = [120,60,150] 
		self.boundaries = [(self.HSV_low, self.HSV_high)]

		self.existing_directory = os.getcwd()
		
		# creating a new file to save the positions

		self.path = "/home/milleniumfalcon/"

		#USER PARAMETERS
		#x = 2.17, y = -0.06, z = -0.28 
		now = datetime.now()
		dt_string = now.strftime("%d%m%Y %H%M%S")
		self.folder_name = dt_string
		self.img_avg = 20

		self.folder_path = self.path + self.folder_name + "/"
		self.pics_path = self.folder_path + "Pics/"
		self.proc_path = self.folder_path + "Processed_Pics/"

		
		os.mkdir(self.folder_path)
		os.mkdir(self.pics_path)
		os.mkdir(self.proc_path)
		self.doc_string = self.folder_path + "positions.csv"
		positions = open(self.doc_string,"w")
		positions.write("Index, Time, X, Y, Z, Q1, Q2, Q3, Q4 \n")
		positions.close()

		self.actual_pos = np.zeros((1,6)) #initialize actual position 
		self.actual_rot = np.zeros((1,4)) #initialize actual orientation

		self.prev_time = 0
		self.img_count = 0
		self.use_count = 0
		self.hoop_coords = np.zeros((3,1))
		self.hazard = 0
		self.pub = rospy.Publisher("/hoop_pos",PoseStamped,queue_size = 10) #publisher
		

	def pub_msg(self,pose, pub_bool):
		msg = PoseStamped()
		msg.header.stamp = rospy.Time.now() #time stamp
		msg.header.frame_id = 'map'
		msg.pose.position.x = pose[0] #desired x
		msg.pose.position.y = pose[1] #desired y
		msg.pose.position.z = pose[2] #desired z
		msg.pose.orientation.x = pub_bool

		self.pub.publish(msg)
		return
	
	#subscriber callback for image process
	def image_flag(self,msg):
		if msg.data == 1:  #hoop image process
			#set boolean to true
			self.image_hoop = True
		elif msg.data == 2: #hazard sign image proccess 
			#set boolean to true
			self.image_hazard = True
		return

	# vicon topic functions to obtain 
	def _vicon_callback(self,msg):
		self._vicon_msg = msg
	
	def pub_hazard(self,side):
		if side == 1:
			side_str = 'left'
		elif side == 2:
			side_str = 'right'
			
		self.pub_hazard_sign.publish(side_str)

	def get_position(self):
		self.actual_pos[0,0] = self._vicon_msg.transform.translation.x
		self.actual_pos[0,1] = self._vicon_msg.transform.translation.y
		self.actual_pos[0,2] = self._vicon_msg.transform.translation.z

		self.actual_rot[0,0] = self._vicon_msg.transform.rotation.x
		self.actual_rot[0,1] = self._vicon_msg.transform.rotation.y
		self.actual_rot[0,2] = self._vicon_msg.transform.rotation.z
		self.actual_rot[0,3] = self._vicon_msg.transform.rotation.w
		
		quaternion = np.array([self.actual_rot[0,0],
							  self.actual_rot[0,1],
							  self.actual_rot[0,2],
							  self.actual_rot[0,3]])
		
		# Determine the euler angles
		euler = euler_from_quaternion(quaternion)
		roll = euler[0]
		pitch = euler[1]
		yaw = euler[2]
		
		self.actual_pos[0,3] = roll
		self.actual_pos[0,4] = pitch
		self.actual_pos[0,5] = yaw
		

	# Called every CONNECTION_CHECK_PERIOD ms, if we haven't received anything since the last callback, will assume we are having network troubles and display a message in the status bar
	def ConnectionCallback(self):
		self.connected = self.communicationSinceTimer
		self.communicationSinceTimer = False

	def RedrawCallback(self):
		if self.image is not None:
			# We have some issues with locking between the display thread and the ros messaging thread due to the size of the image, so we need to lock the resources
			self.imageLock.acquire()
			try:			
					# Convert the ROS image into a QImage which we can display
					if self.processImages == False:
						image = QtGui.QPixmap.fromImage(QtGui.QImage(self.image.data, self.image.width, self.image.height, QtGui.QImage.Format_RGB888))						
					# display processed image when processing is enabled
					else:
						if self.cv_output is not None:					
							# convert from openCV output cv_output image back to ROS image (Optional for visualization purposes)
							img_msg = self.bridge.cv2_to_imgmsg(self.cv_output, encoding="bgr8")
							# convert to QImage to be displayed
							image = QtGui.QPixmap.fromImage(QtGui.QImage(img_msg.data, img_msg.width, img_msg.height, QtGui.QImage.Format_RGB888))
						else:
							image = QtGui.QPixmap.fromImage(QtGui.QImage(self.image.data, self.image.width, self.image.height, QtGui.QImage.Format_RGB888))		
					
			finally:
				self.imageLock.release()

			# We could  do more processing (eg OpenCV) here if we wanted to, but for now lets just display the window.
			self.resize(image.width(),image.height())
			self.imageBox.setPixmap(image)

		# Update the status bar to show the current drone status & battery level
		self.statusBar().showMessage(self.statusMessage if self.connected else self.DisconnectedMessage)
		self.pub_keyboard_alive.publish(1)
		
		

	def ReceiveImage(self,data):
		# Indicate that new data has been received (thus we are connected)
		self.communicationSinceTimer = True

		# We have some issues with locking between the GUI update thread and the ROS messaging thread due to the size of the image, so we need to lock the resources
		self.imageLock.acquire()
		try:
			self.image = data # Save the ros image for processing by the display thread
			# 2017-03-22 we do not recommend saving images in this function as it might cause huge latency
		finally:
			self.imageLock.release()
			
	# 2017-03-22 sample function of saving images.
	# TODO feel free to modify, you could use a timer to capture the image at a certain rate, or modify the keyboard_controller.py to capture the image through a key
	def SaveImage(self, event):
		image_saved = False
		# ensure not in the process of acquiring image
		if self.imageLock.acquire():
			try:
				# first checking that image capturing has started and that we are not referencing a NULL Assignment
				if not(self.image is None):
					# getting the time from ROS
					self.prev_time = rospy.get_time()

					# convert from ROS image to OpenCV image
					cv_img = self.bridge.imgmsg_to_cv2(self.image, desired_encoding="bgr8")

					image_name =  self.pics_path + str(self.img_count) + ".png"
					#print(image_name)
					image_proc_name = self.proc_path + "processed_" + str(self.img_count) + ".png"
					cv2.imwrite(image_name, cv_img)
					image_saved = True
					
	
					
					# updating vicon data so that it can be used below
					self.get_position()
					
					# writing in the text string that will form one row of the CSV positions file
					text_string = str(self.img_count) + ", "
					text_string = text_string + str((self.prev_time)) + ", "
					text_string = text_string + str(self.actual_pos[0,0]) + ", "
					text_string = text_string + str(self.actual_pos[0,1]) + ", "
					text_string = text_string + str(self.actual_pos[0,2]) + ", "
					text_string = text_string + str(self.actual_rot[0,0]) + ", "
					text_string = text_string + str(self.actual_rot[0,1]) + ", "
					text_string = text_string + str(self.actual_rot[0,2]) + ", "
					text_string = text_string + str(self.actual_rot[0,3]) + "\n"

					# writing the string to the csv file and closing it
					positions = open(self.doc_string,"a")
					positions.write(text_string)
					positions.close()
	
					

					self.img_count = self.img_count + 1				

			except CvBridgeError as e:
				print "Image conversion failed: %s" % e
			# TODO use cv2.imwrite function to write images to local disk
			# might want to consider storing the corresponding vehicle attitude/position to geolocate target
			self.imageLock.release()
		if (image_saved) and (self.image_hoop or self.image_hazard):
				image_read = cv2.imread(image_name)
				
				if (self.image_hoop):
					
					(D_frame, use_data) = hoopDetect(image_proc_name ,image_read, self.boundaries)
					if (use_data):
						self.hoop_coords = self.hoop_coords + D_frame
						self.use_count = self.use_count + 1

					if (((self.use_count%self.img_avg) == 0) and (self.use_count != 0)):
						self.hoop_coords = (self.hoop_coords/(self.use_count))
						self.use_count = 0
						#print(self.hoop_coords)
				
						pub_bool = 1	
						if (self.hoop_coords[0] < .5):
							pub_bool = 0
					
						quaternion = np.array([self.actual_rot[0,0], self.actual_rot[0,1],self.actual_rot[0,2],self.actual_rot[0,3]])
						R_d2w = np.transpose((quaternion_matrix(quaternion))) 
						R_d2w = R_d2w[0:3,0:3]
						hoop_rot_only = R_d2w*np.mat(self.hoop_coords)
						absolute_hoop = hoop_rot_only + np.transpose(np.mat([self.actual_pos[0,0], self.actual_pos[0,1], self.actual_pos[0,2]]))


						self.pub_msg(np.array(absolute_hoop), pub_bool)


						self.hoop_coords = np.zeros((3,1))
				
						print(absolute_hoop)
						self.image_hoop = False
						
						
				if (self.image_hazard):
					
					
					(side, use_data) = hazardDetect(image_proc_name, image_read)
					if (use_data):
						self.hazard = self.hazard + side
						self.use_count = self.use_count + 1

					if (((self.use_count%self.img_avg) == 0) and (self.use_count != 0)):
						self.hazard = round(self.hazard/float(self.use_count))
						self.use_count = 0
						#print(self.hoop_coords)
				
						self.pub_hazard(self.hazard)
						
						print(self.hazard)
						self.hazard = 0
						self.image_hazard = False
		
				        
	# 2017-03-31 Lab 4 code ================================
	# the codes written here serve as a guideline, it is not required that you use the code. Feel free to modify.

	def EnableImageProcessing(self):  # called from KeyboardController Key_P
		self.processImages = True

	def DisableImageProcessing(self): # called from KeyboardController Key_P
		self.processImages = False

	def HoopDetector(self, event):
		image_saved = False
		# reference: http://www.pyimagesearch.com/2014/08/04/opencv-python-color-detection/
		if ((self.processImages) and (self.imageLock.acquire())):
			try:
				if not(self.image is None):
					# getting the time from ROS
					self.prev_time = rospy.get_time()

					# convert from ROS image to OpenCV image
					cv_img = self.bridge.imgmsg_to_cv2(self.image, desired_encoding="bgr8")

					image_name =  self.pics_path + str(self.img_count) + ".png"
					#print(image_name)
					image_proc_name = self.proc_path + "processed_" + str(self.img_count) + ".png"
					cv2.imwrite(image_name, cv_img)
					image_saved = True
					
	
					
					# updating vicon data so that it can be used below
					self.get_position()
					'''
					# writing in the text string that will form one row of the CSV positions file
					text_string = str(img_count) + ", "
					text_string = text_string + str((self.prev_time)) + ", "
					text_string = text_string + str(self.actual_pos[0,0]) + ", "
					text_string = text_string + str(self.actual_pos[0,1]) + ", "
					text_string = text_string + str(self.actual_pos[0,2]) + ", "
					text_string = text_string + str(self.actual_pos[0,3]) + ", "
					text_string = text_string + str(self.actual_pos[0,4]) + ", "
					text_string = text_string + str(self.actual_pos[0,5])

					# writing the string to the csv file and closing it
					positions = open(self.doc_string,"a")
					positions.write(text_string)
					positions.close()
	
					'''

					self.img_count = self.img_count + 1

			except CvBridgeError as e:
				print "Image conversion failed: %s" % e
			self.imageLock.release()
			# carry out color thresholding

			if (image_saved):
				image_read = cv2.imread(image_name)
				
				(D_frame, use_data) = hoopDetect(image_proc_name ,image_read, self.boundaries)
				if (use_data):
					self.hoop_coords = self.hoop_coords + D_frame
					self.use_count = self.use_count + 1

			if ((self.use_count%self.img_avg) == 0):
				self.hoop_coords = (self.hoop_coords/(self.use_count+1))
				self.use_count = 0
				#print(self.hoop_coords)
				
				pub_bool = 1
				if (self.hoop_coords[0] < 1.75):
					pub_bool = 0
				quaternion = np.array([self.actual_rot[0,0], self.actual_rot[0,1],self.actual_rot[0,2],self.actual_rot[0,3]])
				R_d2w = np.transpose((quaternion_matrix(quaternion))) 
				R_d2w = R_d2w[0:3,0:3]
				hoop_rot_only = R_d2w*np.mat(self.hoop_coords)
				absolute_hoop = hoop_rot_only + np.transpose(np.mat([self.actual_pos[0,0], self.actual_pos[0,1], self.actual_pos[0,2]]))


				self.pub_msg(np.array(absolute_hoop), pub_bool)

				# publish self.hoop_coords here

				self.hoop_coords = np.zeros((3,1))
				print(absolute_hoop)



			
	
	def ProcessHoopImage(self,mask):
		return None
		# TODO algorithm to detect center of circular hoop based on pixel locations
		# suggested method to obtain pixel locations using numpy
		# https://docs.scipy.org/doc/numpy/reference/generated/numpy.where.html
		# px_locations = np.where( mask > 0 )
		# will return (array_of_row_index, array_of_col_index) of the corresponding pixel location

	# 2017-03-31 ============================================

	def ReceiveNavdata(self,navdata):
		# Indicate that new data has been received (thus we are connected)
		self.communicationSinceTimer = True

		# Update the message to be displayed
		msg = self.StatusMessages[navdata.state] if navdata.state in self.StatusMessages else self.UnknownMessage
		self.statusMessage = '{} (Battery: {}%)'.format(msg,int(navdata.batteryPercent))

		self.tagLock.acquire()
		try:
			if navdata.tags_count > 0:
				self.tags = [(navdata.tags_xc[i],navdata.tags_yc[i],navdata.tags_distance[i]) for i in range(0,navdata.tags_count)]
			else:
				self.tags = []
		finally:
			self.tagLock.release()


# since this file is not called in launch, the main function has been left unchanged from
# the starter code
if __name__=='__main__':
	import sys
	rospy.init_node('ardrone_video_display')
	app = QtGui.QApplication(sys.argv)
	display = DroneVideoDisplay()
	display.show()
	status = app.exec_()
	rospy.signal_shutdown('Great Flying!')
	sys.exit(status)
