#import the necessary packages
import cv2
import numpy as np
import glob
import os



#Loading all images


#Creating window
#cv2.namedWindow('Colorbars')
#cv2.namedWindow('Harris Parameters')

#Setting names for trackbars - HSV STUFF
hh='Hue High'
hl='Hue Low'
sh='Saturation High'
sl='Saturation Low'
vh='Value High'
vl='Value Low'
er='Erode'
dil='dilate'

wnd = 'Colorbars'
#------------------------------------#

ct = 'Color Tresh?'
bs = 'blockSize - neighbour considered'
ks = 'ksize - aperture parameter sobel'
kp = 'k - Harris free parameter'

wnd2 = 'Harris Parameters'

#Hazard arrow stuff
HSV_edge = [0,161,100]
HSV_center = [179,255,255]

erode = 2
dilate = 0

def nothing(x):
	pass


'''

#Begin Creating trackbars for each
cv2.createTrackbar(hl,  wnd, HSV_edge[0],   179, nothing)
cv2.createTrackbar(hh,  wnd, HSV_center[0], 179, nothing)
cv2.createTrackbar(sl,  wnd, HSV_edge[1],   255, nothing)
cv2.createTrackbar(sh,  wnd, HSV_center[1], 255, nothing)
cv2.createTrackbar(vl,  wnd, HSV_edge[2],   255, nothing)
cv2.createTrackbar(vh,  wnd, HSV_center[2], 255, nothing)
cv2.createTrackbar(er,  wnd, erode,          10, nothing)
cv2.createTrackbar(dil, wnd, dilate,         10, nothing)

cv2.createTrackbar(ct, wnd2, 1,         90, nothing) #div by 10
cv2.createTrackbar(bs, wnd2, 3,         20, nothing)
cv2.createTrackbar(ks, wnd2, 10,         50, nothing)
cv2.createTrackbar(kp, wnd2, 0,         500, nothing) #div by 100
'''




def hazardDetect(proc_name, imaget):

	image = imaget.copy()
	use_data = True
 

	hsv_img = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
	hsv_img2 = cv2.cvtColor(arimage, cv2.COLOR_BGR2HSV)

	'''

	hul  = cv2.getTrackbarPos(hl,  wnd)#0
	huh  = cv2.getTrackbarPos(hh,  wnd)#179
	sal  = cv2.getTrackbarPos(sl,  wnd)#161
	sah  = cv2.getTrackbarPos(sh,  wnd)#255
	val  = cv2.getTrackbarPos(vl,  wnd)#100
	vah  = cv2.getTrackbarPos(vh,  wnd)#255
	ero  = cv2.getTrackbarPos(er,  wnd)#2
	dilt = cv2.getTrackbarPos(dil, wnd)#0

	'''
	
	hul  = 0
	huh  = 179
	sal  = 161
	sah  = 255
	val  = 100
	vah  = 255
	ero  = 2
	dilt = 0

	

	
	cthresh = 1
	bsi = 3
	ksi = 10
	kfr = 0

	'''

	cthresh= cv2.getTrackbarPos(ct, wnd2)
	bsi    = cv2.getTrackbarPos(bs, wnd2) 
	ksi    = cv2.getTrackbarPos(ks, wnd2) 
	kfr	   = cv2.getTrackbarPos(kp, wnd2)
	'''
	


	#HSV Stuff for green targets
	HSVLOW=np.array([hul,sal,val])
	HSVHIGH=np.array([huh,sah,vah])
	boundaries = [(HSVLOW, HSVHIGH)]

	mask = cv2.inRange(hsv_img, HSVLOW, HSVHIGH)
	mask = cv2.erode(mask, None, iterations=ero)
	mask = cv2.dilate(mask, None, iterations=dilt)
	output = cv2.bitwise_and(image, hsv_img, mask = mask)

	output_bgr = cv2.cvtColor(output, cv2.COLOR_HSV2BGR)
	imgray = cv2.cvtColor(output_bgr,cv2.COLOR_BGR2GRAY)

		#--------------------------------------#

	#--------------------LINES DETECT------------#

	edges = cv2.Canny(imgray, ksi, kfr,apertureSize = 3)

	lines = cv2.HoughLines(edges, 0.68, np.pi/180, 13)

	#------------------------#

	x1s=np.zeros(0)
	x2s=np.zeros(0)
	y1s=np.zeros(0)
	y2s=np.zeros(0)

	if lines is not None:
		for rho, theta in lines[0]:
			a = np.cos(theta)
			b = np.sin(theta)
			x0 = a*rho
			y0 = b*rho
			x1 = int(x0 + 1000*(-b))
			y1 = int(y0 + 1000*(a))
			x2 = int(x0 - 1000*(-b))
			y2 = int(y0 - 1000*(a))

			x1s = np.concatenate((x1s,x1), axis=None)
			y1s = np.concatenate((y1s,y1), axis=None)
			x2s = np.concatenate((x2s,x2), axis=None)
			y2s = np.concatenate((y2s,y2), axis=None)
	

	xt1=np.zeros(0)
	yt1=np.zeros(0)
	xt2=np.zeros(0)
	yt2=np.zeros(0)

	xt1_2=np.zeros(0)
	yt1_2=np.zeros(0)
	xt2_2=np.zeros(0)
	yt2_2=np.zeros(0)

	xt1_3=np.zeros(0)
	yt1_3=np.zeros(0)
	xt2_3=np.zeros(0)
	yt2_3=np.zeros(0)

	xt1_4=np.zeros(0)
	yt1_4=np.zeros(0)
	xt2_4=np.zeros(0)
	yt2_4=np.zeros(0)

	n=600

	if x1s is not None:
		for k in zip(x1s,y1s,x2s,y2s):
			poots = (k[3]-k[1])/((k[2]-k[0])+0.0001)

			if (len(xt1)==0) or (abs(k[0]-np.mean(xt1[0])) < n and abs(k[1]-np.mean(yt1[0])) < n  and abs(k[2]-np.mean(xt2[0])) < n  and abs(k[3]-np.mean(yt1[0])) < n):
				if abs(poots) > 4:
					xt1=np.concatenate((xt1,k[0]), axis=None)
					yt1=np.concatenate((yt1,k[1]), axis=None)
					xt2=np.concatenate((xt2,k[2]), axis=None)
					yt2=np.concatenate((yt2,k[3]), axis=None)
					continue

			if len(xt1_2)==0 or (abs(k[0]-np.mean(xt1_2[0])) < n and abs(k[1]-np.mean(yt1_2[0])) < n  and abs(k[2]-np.mean(xt2_2[0])) < n  and abs(k[3]-np.mean(yt1_2[0])) < n):
				if abs(poots) < 0.4:
					xt1_2=np.concatenate((xt1_2,k[0]), axis=None)
					yt1_2=np.concatenate((yt1_2,k[1]), axis=None)
					xt2_2=np.concatenate((xt2_2,k[2]), axis=None)
					yt2_2=np.concatenate((yt2_2,k[3]), axis=None)
					continue

			if len(xt1_3)==0 or (abs(k[0]-np.mean(xt1_3[0])) < n and abs(k[1]-np.mean(yt1_3[0])) < n  and abs(k[2]-np.mean(xt2_3[0])) < n  and abs(k[3]-np.mean(yt1_3[0])) < n):
				if poots < 0 and poots > -1.3:
					xt1_3=np.concatenate((xt1_3,k[0]), axis=None)
					yt1_3=np.concatenate((yt1_3,k[1]), axis=None)
					xt2_3=np.concatenate((xt2_3,k[2]), axis=None)
					yt2_3=np.concatenate((yt2_3,k[3]), axis=None)
					continue

			if len(xt1_4)==0 or (abs(k[0]-np.mean(xt1_4[0])) < n and abs(k[1]-np.mean(yt1_4[0])) < n  and abs(k[2]-np.mean(xt2_4[0])) < n  and abs(k[3]-np.mean(yt1_4[0])) < n):		
				if poots > 0 and poots < 1.3:
					xt1_4=np.concatenate((xt1_4,k[0]), axis=None)
					yt1_4=np.concatenate((yt1_4,k[1]), axis=None)
					xt2_4=np.concatenate((xt2_4,k[2]), axis=None)
					yt2_4=np.concatenate((yt2_4,k[3]), axis=None)
					continue


	test = [int(np.mean(xt1)), int(np.mean(yt1)), int(np.mean(xt2)), int(np.mean(yt2))]
	test2 = [int(np.mean(xt1_2)), int(np.mean(yt1_2)), int(np.mean(xt2_2)), int(np.mean(yt2_2))]
	test3 = [int(np.mean(xt1_3)), int(np.mean(yt1_3)), int(np.mean(xt2_3)), int(np.mean(yt2_3))]
	test4 = [int(np.mean(xt1_4)), int(np.mean(yt1_4)), int(np.mean(xt2_4)), int(np.mean(yt2_4))]

	cv2.line(image,(test[0],test[1]),(test[2],test[3]),(0,255,0),2)
	cv2.line(image,(test2[0],test2[1]),(test2[2],test2[3]),(0,255,0),2)
	cv2.line(image,(test3[0],test3[1]),(test3[2],test3[3]),(0,255,0),2)
	cv2.line(image,(test4[0],test4[1]),(test4[2],test4[3]),(0,255,0),2)

	ms1 = (np.mean(yt2_3)-np.mean(yt1_3))/(np.mean(xt2_3)-np.mean(xt1_3))
	ms2 = (np.mean(yt2_4)-np.mean(yt1_4))/(np.mean(xt2_4)-np.mean(xt1_4))
	msu = (np.mean(yt2)-np.mean(yt1))/(np.mean(xt2)-np.mean(xt1)+0.001)

	cs1 = yt2_3 - ms1*xt2_3
	cs2 = yt2_4 - ms2*xt2_4
	csu = yt1 - msu*xt1

	xintersec = (cs2-cs1)/(ms1-ms2)
	yintersec = xintersec*ms1+cs1

	cv2.circle(image, (xintersec, yintersec), 5, (0,255,255),2)
	xvert = (yintersec-csu)/msu

	cv2.circle(image, (xvert,yintersec), 5, (0,255,255),2)

	cv2.imwrite(proc_name, image)

	if ((len(xt1)== 0) or (len(xt1_3) == 0) or (len(xt1_4) == 0)):
		use_data = False

	if xvert > xintersec: 
		return (1,use_data)
	else:
		return (2,use_data)
	#----------------------------#


	#----------------------------------------------#

	

	



	# if ch != 32 and ch != -1:
	# 	print("---------END PARAMETERS---------")
	# 	print("HSV_LOW: " + str(HSVLOW))
	# 	print("HSV_HIGH: " + str(HSVHIGH))
	# 	print("Erode: " + str(ero))
	# 	print("Dilate: " + str(dilt))
	# 	break


