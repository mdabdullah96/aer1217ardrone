#!/usr/bin/env python

## Simple talker demo that published std_msgs/Strings messages
## to the 'chatter' topic

import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped

def talker():
	pub = rospy.Publisher('/desired_trajectory', String, queue_size=10)
	pub_hoop = rospy.Publisher('/hoop_pos', PoseStamped, queue_size=10)
	pub_error = rospy.Publisher('/error', PoseStamped, queue_size=10)
	rospy.init_node('talker', anonymous=True)
	# rate = rospy.Rate(0.5) # 10 seconds
	# rate1 = rospy.Rate(0.1) # 5 seconds
	# pub_flag = True
	#print("Message: ")
	# hello_str = "linear"
	# while not rospy.is_shutdown():
	# 	#hello_str = "hello world %s" % rospy.get_time()
	# 	#rospy.loginfo(hello_str)
	# 	if pub_flag:
	# 	  rate.sleep()
	# 	  pub.publish(hello_str)
	# 	  rate1.sleep()
	# 	  msg = PoseStamped()
	#   	msg.header.stamp = rospy.Time.now() #time stamp
	#   	msg.header.frame_id = 'map'
	#   	msg.pose.position.x = 0 #desired x
	#   	msg.pose.position.y = 0 #desired y
	#   	msg.pose.position.z = 0 #desired z
	#   	msg.pose.orientation.x = 1 #flag
	#   	pub_error.publish(msg)
		# pub = False
		# rospy.spin()
	#rate.sleep()
	#rospy.loginfo(hello_str)
	#pub.publish(hello_str)
	msg = PoseStamped()
	msg.header.stamp = rospy.Time.now() #time stamp
  	msg.header.frame_id = 'map'
  	msg.pose.position.x = 0 #desired x
  	msg.pose.position.y = 0 #desired y
  	msg.pose.position.z = 0 #desired z
  	msg.pose.orientation.x = 0 #flag
  	pub_error.publish(msg)
if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException:
		pass
