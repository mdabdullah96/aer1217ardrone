#!/usr/bin/env python2

"""ROS Node for controlling the ARDrone 2.0 using the ardrone_autonomy package.

This ROS node subscribes to the following topics:
/vicon/ARDroneCarre/ARDroneCarre

The node should also subscribe to the desired position topic.

This ROS node publishes to the following topics:
/cmd_vel
/land
/takeoff

"""
import cv2

def nothing(x):
	pass


#Trackbar stuff
hh='KP - XY'
hl='KI - XY'
sh='KD - XY'
wnd = 'Tuning'
cv2.namedWindow('Tuning')
cv2.createTrackbar(hh,  wnd, 1,   10, nothing)
cv2.createTrackbar(hl,  wnd, 0, 5, nothing)
cv2.createTrackbar(sh,  wnd, 6,   15, nothing)

while True:
	cv2.waitKey(1)