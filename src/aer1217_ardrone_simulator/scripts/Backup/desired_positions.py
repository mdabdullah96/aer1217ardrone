#!/usr/bin/env python2

"""ROS Node for publishing desired positions."""

from __future__ import division, print_function, absolute_import

# Import ROS libraries
import roslib
import rospy
import numpy as np
import math
from geometry_msgs.msg import Point, PoseStamped
from tf.transformations import quaternion_from_euler,euler_from_quaternion
from std_msgs.msg import String, Int8


class ROSDesiredPositionGenerator(object):
	"""ROS interface for publishing desired positions."""
	# write code here for desired position trajectory generator
	def __init__(self):
	#-----------------------Constants-----------------------#
		self.start = np.zeros((3,1)) #start point
		self.end = np.zeros((3,1)) #end point
		self.center = np.zeros((2,1)) #center point
		self.radius = 0 #radius
		self.time = 0 #time for forward pass
		self.traj_type = 'sss' #trajectory type
		self.point = np.zeros((3,1)) #current point
		self.vel = np.zeros((3,1)) #velocity
		self.forward = True #forward pass
		self.dist = 9000 #distance from start to end
		self.w = 0 # angular velocity
		self.t_run = 0 #continuous time
		self.yaw_angle = 0 #yaw_angle
		self.time_waypoints = np.zeros((3,1)) #waypoint list of times of each segment
		self.segment_count = 0 #linear segments completed
		self.num_action_point = 0 #no. of action points completed
		self.cmnd_send = True #flag to send image processing command
		self.waypoint_error = 100 #waypoint error norm
		self.yaw_error = 3 #yaw error
		self.move = False #flag to start moving from start waypoint
		self.start_time = rospy.get_time() #timer to ensure uav does not get stuck reaching error threshold
		self.escape_time = 10 #time threshold to escape
		self.obstacle_time = rospy.get_time() #intialize obstacle time
		self.obstacle_thrsh = 20 #time threshold to escape if hoop or hazard detect throws an error

	#-----------------------CHANGE THESE BASED ON ROUTE-----------------------#
		
		#survey Phase 1
		# self.waypoints = np.array([[-1.8000,2.4000,1.2500],[-1.8000,-1.5000,1.2500],[-0.9750,-1.5000,1.2500],[-0.9750,2.4000,1.2500],[-0.1500,2.4000,1.2500],[-0.1500,-1.5000,1.2500],[0.6750,-1.5000,1.2500],[0.6750,2.4000,1.2500],[1.5000,2.4000,1.2500],[1.5000,-1.5000,1.2500]]) #set of all waypoints to visit
		# self.time_waypoints = np.array([13,2.75,13,2.75,13,2.75,13,2.75,13]) #set of times of trajectory in forward pass per linear segment
		# self.hold_waypoint = np.zeros(np.shape(self.waypoints)[0]) #don't want to hold at any waypoints

		#obstacle course Phase 2
		self.waypoints = np.array([[-1.8400,0.3500,1.3500],[-0.4000,-0.2300,1.6500],[0.4100,0.1300,1.3500],[0.0800,0.9100,1.8000],[0.7800,2.0500,1.2000],[0.0000,0.0000,0.0000],[0.0000,0.0000,0.0000],[0.0000,0.0000,0.0000],[1.3400,-0.3300,1.5000],[0.1261,-1.0275,1.5000],[-1.5100,-1.1200,1.0500],[0.0000,0.0000,0.0000],[0.0000,0.0000,0.0000],[0.0000,0.0000,0.0000],[-1.3400,1.2600,1.0500],[-0.0589,1.8245,1.0500],[0.7800,2.0500,0.5000],[0.0800,0.9100,0.5000],[-2.0000,2.0000,0.5000]]) 
		self.time_waypoints = np.array([5.27,3.12,3.2,4.89,2.67,6.5,2.67,2.75,4.67,5.66,2.67,6.5,2.67,2.53,4.67,3.43,4.46,7.83]) #set of times of trajectory in forward pass per linear segment
		self.hold_waypoint = np.array([0,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0]) #flag to hold at action waypoints
		self.yaw_angle_fixed = np.array([-1.61,-2.62,1.21,0.415]) #fixed yaw angle for action waypoints [rad]
		self.image_type = np.array([2,1,2,1]) #action waypoint order. 1-hoop 2-hazard sign
		hazards = np.where(self.image_type == 2)
		self.first_haz = hazards[0][0] #action point number of first hazard sign
		self.second_haz = hazards[0][1] #action point number of second hazard sign
		box_first = np.array([[-0.0194,2.0814,1.2000],[1.5794,2.0186,1.2000],[-0.0959,0.1329,1.2000],[1.5028,0.0701,1.2000],[0.7034,0.1015,1.2000],[0.7034,0.1015,1.2000]]) #box maneuver for first hazard sign
		box_second = np.array([[-0.7615,-1.4024,1.0500],[-2.2585,-0.8376,1.0500],[-0.0732,0.4221,1.0500],[-1.5702,0.9869,1.0500],[-0.8217,0.7045,1.0500],[-0.8217,0.7045,1.0500]]) #box maneuver for second hazard sign
		self.box_dict = {'first':box_first,'second':box_second} # dictionary of maneuvers
		

	#-----------------------Subscriber/Publisher-----------------------#
		self.pub = rospy.Publisher("/des_pos", PoseStamped, queue_size = 10) #publisher for desired positions
		self.pub_image = rospy.Publisher("/image_process", Int8, queue_size = 10) #publisher for image process type at action waypoints
		self.hoop_topic = rospy.Publisher("/hoop_pos", PoseStamped, queue_size = 10) #publisher for hoop pos action waypoints
		self.hazard_topic = rospy.Publisher("/hazard", String, queue_size = 10) #publisher for hazard action waypoints

		rospy.Subscriber("/desired_trajectory", String, self.user_info) #subscriber
		rospy.Subscriber("/hoop_pos",PoseStamped,self.hoop_traj) #subscriber
		rospy.Subscriber("/hazard",String,self.hazard_traj) #subscriber
		rospy.Subscriber("/error",PoseStamped,self.point_error) #subscriber
		return

	def pub_msg(self,pose):
		msg = PoseStamped()
		msg.header.stamp = rospy.Time.now() #time stamp
		msg.header.frame_id = 'map'
		msg.pose.position.x = pose[0] #desired x
		msg.pose.position.y = pose[1] #desired y
		msg.pose.position.z = pose[2] #desired z
		quat = quaternion_from_euler(0,0,pose[3]) #desired yaw angle as quaternion
		msg.pose.orientation.x = quat[0]
		msg.pose.orientation.y = quat[1]
		msg.pose.orientation.z = quat[2]
		msg.pose.orientation.w = quat[3]
		self.pub.publish(msg)
		return
	
	#Publish the type of obstacle so image processing algorithm knows what to look for
	def pub_image_type(self):
		if self.image_type[self.num_action_point] == 1: #1 for hoop
			rospy.loginfo('Looking for hoop.')
		elif self.image_type[self.num_action_point] == 2:#2 for hazard
			rospy.loginfo('Reading hazard sign.')

		self.pub_image.publish(self.image_type[self.num_action_point])

	#Failsafe publisher to ensure drone moves past obstacle in the event of error
	def pub_escape(self):
		#figure out the type of obstacle first and then publish appropriate message
		if self.image_type[self.num_action_point] == 1: #1 for hoop
			msg = PoseStamped()
			msg.header.stamp = rospy.Time.now() #time stamp
			msg.header.frame_id = 'map'
			msg.pose.position.x = 0 #desired x
			msg.pose.position.y = 0 #desired y
			msg.pose.position.z = 1.5 #desired z
			msg.pose.orientation.x = 1 #flag
			self.hoop_topic.publish(msg)

		elif self.image_type[self.num_action_point] == 2:#2 for hazard
			msg = 'left'
			self.hazard_topic.publish(msg)

	#Obtain trajectory information
	def user_info(self,msg):
		self.traj_type = msg.data

		if self.traj_type == 'linear':

			self.time = 30 #time of trajectory in forward pass
			self.start = np.array([-2,2,1.8]) #start point
			self.point = np.copy(self.start)
			self.end = np.array([1.5,-1.5,1.8]) #end point
			diff = self.end - self.start
			self.vel = diff/self.time #time based velocity
			self.dist = np.linalg.norm(diff) #Eucledian distance

		elif self.traj_type == 'circular':
			
			self.time = 15 #time to complete 1 circle
			self.radius = 1 #radius
			self.sprials = 0 #no. of spirals
			self.center = np.array([0,1,1.5]) #center location
			self.w = 2*np.pi/self.time # angular velocity
			#self.vel = (1.5-0.5)/(self.time*self.sprials) #velocity in z
			self.vel = 0 #velocity in z =0 for closed circle

		elif self.traj_type == 'survey':
			#Leftover from Lab3

			#S Survey
			#self.time_waypoints = np.array([10,5,10,5,10,20]) #set of times of trajectory in forward pass per linear segment
			#Obstacle
			rospy.loginfo('Beginning')
			
		else:
			self.traj_type = '' #trajectory type
		return
	
	#call back function to create a hoop trajectory
	def hoop_traj(self,msg):
		hoop_x = msg.pose.position.x #hoop x
		hoop_y = msg.pose.position.y #hoop y
		hoop_z = msg.pose.position.z #hoop z
		if msg.pose.orientation.x: #in the event we want to go through the hoop
			self.point[2] = hoop_z #set current z to match hoop center
			self.end[2] = hoop_z #set end z to match hoop center
			self.waypoints[self.segment_count,2] = hoop_z #correct waypoint current z to match hoop center
			diff = self.end - self.start
			self.dist = np.linalg.norm(diff) #New Eucledian distance
			rospy.loginfo('Hoop detected at '+ str(hoop_z) + 'm!')
			self.hold_waypoint[self.segment_count-1] = 0 #no longer want to hold at waypoint
			self.move = True #move forward now without checking error 
			self.waypoint_error = 100 #reset error
			self.num_action_point +=1
		return 
	
	#call back function to create a hazard avoidence trajectory
	def hazard_traj(self,msg):
		hazard_dirn = msg.data
		if hazard_dirn == 'left':
			#set dummy waypoints to move drone left
			#check if first hazard or second
			if self.num_action_point == self.first_haz: #if first
				traj_avoid = self.box_dict['first'] #box manuevers for first hazard
				#set waypoints left, forward and opposite to be box[1,3,5]
				self.waypoints[[self.segment_count,self.segment_count+1,self.segment_count+2],:] = traj_avoid[[1,3,5],:]
				self.end = self.waypoints[self.segment_count] #set current end point from 0,0,0 to left
				#recompute velocity and norm as the dummy index is now replaced
				diff = self.end - self.start
				self.vel = diff/self.time_waypoints[self.segment_count-1] #time based velocity for particular segment
				self.dist = np.linalg.norm(diff) #Eucledian distance

			else: #second hazard
				traj_avoid = self.box_dict['second'] #box manuevers for second hazard
				#set waypoints left, forward and opposite to be box[1,3,5]
				self.waypoints[[self.segment_count,self.segment_count+1,self.segment_count+2],:] = traj_avoid[[1,3,5],:]
				self.end = self.waypoints[self.segment_count] #set current end point from 0,0,0 to left
				#recompute velocity and norm as the dummy index is now replaced
				diff = self.end - self.start
				self.vel = diff/self.time_waypoints[self.segment_count-1] #time based velocity for particular segment
				self.dist = np.linalg.norm(diff) #Eucledian distance
		else:
			#set dummy waypoints to move drone right 
			#check if first hazard or second
			if self.num_action_point == self.first_haz: #if first
				traj_avoid = self.box_dict['first'] #box manuevers for first hazard
				#set waypoints right, forward and opposite to be box[0,2,4]
				self.waypoints[[self.segment_count,self.segment_count+1,self.segment_count+2],:] = traj_avoid[[0,2,4],:]
				self.end = self.waypoints[self.segment_count] #set current end point from 0,0,0 to right
				#recompute velocity and norm as the dummy index is now replaced
				diff = self.end - self.start
				self.vel = diff/self.time_waypoints[self.segment_count-1] #time based velocity for particular segment
				self.dist = np.linalg.norm(diff) #Eucledian distance
				
			else:
				traj_avoid = self.box_dict['second'] #box manuevers for second hazard
				#set waypoints right, forward and opposite to be box[0,2,4]
				self.waypoints[[self.segment_count,self.segment_count+1,self.segment_count+2],:] = traj_avoid[[0,2,4],:]
				self.end = self.waypoints[self.segment_count] #set current end point from 0,0,0 to right
				#recompute velocity and norm as the dummy index is now replaced
				diff = self.end - self.start
				self.vel = diff/self.time_waypoints[self.segment_count-1] #time based velocity for particular segment
				self.dist = np.linalg.norm(diff) #Eucledian distance
		
		rospy.loginfo('Hazard avoiding!Going '+hazard_dirn)
		self.hold_waypoint[self.segment_count-1] = 0 #no longer want to hold at waypoint
		self.move = True #move forward now without checking error 
		self.waypoint_error = 100 #reset error
		self.num_action_point +=1
		return 

	#call back function to compute waypoint error
	def point_error(self,msg):
		err_x = msg.pose.position.x #error x
		err_y = msg.pose.position.y #error y
		err_z = msg.pose.position.z #error z
		self.waypoint_error = np.linalg.norm([err_x,err_y,err_z]) #error in waypoint

		quat_orientation = np.array([msg.pose.orientation.x,
						  			 msg.pose.orientation.y,
						  			 msg.pose.orientation.z,
						  			 msg.pose.orientation.w])
		euler_orientation = euler_from_quaternion(quat_orientation)
		self.yaw_error = euler_orientation[2] #yaw error
		return 
	
		
	#compute next point in trajectory
	def compute_traj(self,dt):
		#for linear trajectories
		if self.traj_type == 'linear' or self.traj_type == 'survey':
			 	
			#after assigning the start position ensure drone gets there
			if not self.move:
				time_diff =  rospy.get_time() - self.start_time #compute time diff since start point assigned
				if self.waypoint_error > 0.15 and time_diff < self.escape_time: #greater than 15 cm error on point norm and time diff < 10 sec
					self.point = self.point #repeat point tranmission until the error norm is within tolerance
					self.yaw_angle = self.yaw_angle #repeat yaw tranmission until the error norm is within tolerance
				else:

					if not self.hold_waypoint[self.segment_count-1]: #in the event of non action waypoint, proceed normally
						if not (time_diff < self.escape_time): #report when escape timer had to be used
							rospy.loginfo('Had to use escape timer :(')
						self.move = True
						self.waypoint_error = 100
					else:
						if not(self.yaw_angle == self.yaw_angle_fixed[self.num_action_point]):
							self.obstacle_time =  rospy.get_time()
						#send image processing command at the start of obstacle holding 
						if self.cmnd_send and np.absolute(self.yaw_error) <= 0.07 and self.yaw_angle == self.yaw_angle_fixed[self.num_action_point]:
							if not (time_diff < self.escape_time): #report when escape timer had to be used
								rospy.loginfo('Had to use escape timer :(')
							self.pub_image_type()
							self.cmnd_send = False
							
						#hold at point and set yaw to align with arrow
						self.point = self.start
						self.yaw_angle = self.yaw_angle_fixed[self.num_action_point]
						#in the event that we get stuck at an obstacle for any reason for too long. Assume maneuver based on hueristics
						if (rospy.get_time() - self.obstacle_time) > self.obstacle_thrsh:
							rospy.loginfo('Obstacle escape.')
							self.pub_escape()
			
			else:
				self.point = self.point + self.vel*dt #xk = xk-1 + v*dt
				if np.linalg.norm(self.point - self.start) >= self.dist: #until we reach end point
					self.point = np.copy(self.end)
				#set yaw to point at endpoint
				self.yaw_angle =  math.atan2(self.end[1] - self.start[1],self.end[0] - self.start[0])
			

		return np.append(self.point, self.yaw_angle)

if __name__ == '__main__':
	rospy.init_node('desired_position')
	r = ROSDesiredPositionGenerator() #create instance
	freq = 30 # Hz
	rate = rospy.Rate(freq)
	r.segment_count = 0 #waypoint number
	try:
		# publish the trajectory
		while not rospy.is_shutdown():
			if r.traj_type != 'sss': #wait for keypress
				
				#for the survey path set points for each segment until out of waypoints
				if r.traj_type == 'survey' and np.array_equal(r.point,r.end) and r.segment_count < np.size(r.waypoints,0)-1:
					r.start = r.waypoints[r.segment_count] #start point of current segment
					r.point = np.copy(r.start)
					r.end = r.waypoints[r.segment_count + 1] #end point of current segment
					diff = r.end - r.start
					r.vel = diff/r.time_waypoints[r.segment_count] #time based velocity for particular segment
					r.dist = np.linalg.norm(diff) #Eucledian distance
					r.cmnd_send = True
					r.segment_count += 1
					r.move = False
					r.start_time =  rospy.get_time()
				
				point = r.compute_traj(1/freq) #compute point
				#rospy.loginfo(r.traj_type)
				r.pub_msg(point) #publish point
				rate.sleep()
	except KeyboardInterrupt:
		# kill switch, land the drone on interrupt
		data_pnt = Point(0,0,0)
		r.pub_msg(data_pnt)
	rospy.spin()
