#!/usr/bin/env python2

"""ROS Node for controlling the ARDrone 2.0 using the ardrone_autonomy package.

This ROS node subscribes to the following topics:
/vicon/ARDroneCarre/ARDroneCarre

The node should also subscribe to the desired position topic.

This ROS node publishes to the following topics:
/cmd_vel
/land
/takeoff

"""

from __future__ import division, print_function, absolute_import
import sys
import os
# sys.stdout = open("linear_path","w")

# Import ROS libraries
import roslib
import rospy
import numpy as np
import math
from geometry_msgs.msg import Point, PoseStamped
from tf.transformations import quaternion_from_euler

# Import class that computes the desired positions
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import TransformStamped, Twist
from position_controller import PositionController
#from aer1217_ardrone_simulator.msg import DesiredTraj
from std_msgs.msg import Empty

class ROSControllerNode(object):
	"""ROS interface for controlling the Parrot ARDrone in the Vicon Lab."""
	# write code here to define node publishers and subscribers
	# publish to /cmd_vel topic
	# subscribe to /vicon/ARDroneCarre/ARDroneCarre for position and attitude feedback
	
	def __init__(self):
		# =============================================
		# publishers to takeoff, land, cmd_vel, and error
		# =============================================
		self.pub_takeoff = rospy.Publisher('/ardrone/takeoff', Empty, queue_size=1)
		self.pub_land = rospy.Publisher('/ardrone/land', Empty, queue_size=1)
		self.pub_vel = rospy.Publisher('/cmd_vel', Twist, queue_size=16)
		self.pub_err = rospy.Publisher('/error', PoseStamped, queue_size=10)
		# =============================================
		# subscriber to vicon, and desired position
		# =============================================
		self.vicon_topic = '/vicon/ARDroneCarre/ARDroneCarre'
		self._vicon_msg = TransformStamped()
		rospy.Subscriber(self.vicon_topic, TransformStamped, self._vicon_callback)

		self.des_pos_topic = '/des_pos'
		rospy.Subscriber(self.des_pos_topic, PoseStamped, self.update_des_pos)

		self.des_pos = np.zeros((1,4)) #initialize desired position to origin
		self.actual_pos = np.zeros((1,4)) #initialize actual position 
		self.actual_rot = np.zeros((1,4)) #initialize actual orientation
		self.start_control = False


	def output(self):
	#output pose information as list. NOT IN USE
		quaternion = np.array([self.actual_rot[0,0],
							  self.actual_rot[0,1],
							  self.actual_rot[0,2],
							  self.actual_rot[0,3]])
		
		# Determine the euler angles
		euler = euler_from_quaternion(quaternion)
		roll = euler[0]
		pitch = euler[1]
		yaw = euler[2]
		return [self.des_pos[0,0], self.des_pos[0,1], self.des_pos[0,2],0,self.actual_pos[0,0], self.actual_pos[0,1], self.actual_pos[0,2], yaw]


	# -----publisher functions-----
	#publish takeoff
	def takeoff(self):
		self.pub_takeoff.publish()
	
	#publish land
	def land(self):
		self.pub_land.publish()
	
	#publish cmd_vel
	def set_vel(self, phi, theta, climb_rate, yaw_rate):
		msg = Twist()
		msg.linear.x = phi
		msg.linear.y = theta
		msg.linear.z = climb_rate
		msg.angular.z = yaw_rate
		self.pub_vel.publish(msg)
	
	#publish error to /error
	def pub_error(self,err_x,err_y,err_z,err_yaw):
		msg = PoseStamped()
		msg.header.stamp = rospy.Time.now() #time stamp
		msg.header.frame_id = 'map'
		msg.pose.position.x = err_x #desired x
		msg.pose.position.y = err_y #desired y
		msg.pose.position.z = err_z #desired z
		quat = quaternion_from_euler(0,0,err_yaw) #desired yaw angle as quaternion
		msg.pose.orientation.x = quat[0]
		msg.pose.orientation.y = quat[1]
		msg.pose.orientation.z = quat[2]
		msg.pose.orientation.w = quat[3]
		self.pub_err.publish(msg)
		return

	#-----callback functions-----
	#callback for desired position subscriber to update des_pos
	def update_des_pos(self,data):
		self.start_control = True
		self.des_pos[0,0] = data.pose.position.x
		self.des_pos[0,1] = data.pose.position.y
		self.des_pos[0,2] = data.pose.position.z

		quat_orientation = np.array([data.pose.orientation.x,
						  			 data.pose.orientation.y,
						  			 data.pose.orientation.z,
						  			 data.pose.orientation.w])
		euler_orientation = euler_from_quaternion(quat_orientation)
		des_yaw = euler_orientation[2]

		self.des_pos[0,3] = des_yaw
		
	# callback function for vicon (never access this directly)        
	def _vicon_callback(self,msg):
		self._vicon_msg = msg
		
	# function to get actual data from vicon
	def get_position(self):
		self.actual_pos[0,0] = self._vicon_msg.transform.translation.x
		self.actual_pos[0,1] = self._vicon_msg.transform.translation.y
		self.actual_pos[0,2] = self._vicon_msg.transform.translation.z
		
		quaternion = np.array([self.actual_rot[0,0],
							  self.actual_rot[0,1],
							  self.actual_rot[0,2],
							  self.actual_rot[0,3]])
		
		# Determine the euler angles
		euler = euler_from_quaternion(quaternion)
		yaw = euler[2]

		self.actual_pos[0,3] = yaw
		self.actual_rot[0,0] = self._vicon_msg.transform.rotation.x
		self.actual_rot[0,1] = self._vicon_msg.transform.rotation.y
		self.actual_rot[0,2] = self._vicon_msg.transform.rotation.z
		self.actual_rot[0,3] = self._vicon_msg.transform.rotation.w
		

	
# write code to create ROSControllerNode	
if __name__ == '__main__':
	
	# initialize the node and a class instance
	rospy.init_node('ardroneinterface', disable_signals=True)
	drone = ROSControllerNode()
	
	# setup a publishing rate for cmd_vel
	rate = rospy.Rate(100) #100 Hz publish rate for command
	old_time = 0 #initialize time
	
	#DRONE GAINS
	# kp = [0.9,0.9,0.5,4] #(Kpx,Kpy, Kpz, Kp_psi)
	# ki = [0,0] 		   #(Kix, Kiy)
	# kd = [1.5,1.5]	   #(Kdx, Kdy)

	#SIMULATOR GAINS
	kp = [1.5,1.5,2,4] #(Kpx,Kpy, Kpz, Kp_psi)
	ki = [0.3,0.3] 		   #(Kix, Kiy)
	kd = [6,6]	   #(Kdx, Kdy)
	
	
	#instantiate position controller
	p = PositionController(kp,ki,kd)
	desired_output = []
	actual_output = []
	# default error
	err_x = 0
	err_y = 0
	err_z = 0
	err_yaw = 0
	old_time = rospy.get_time() 

	try:
		# publish the trajectory 
		while True:
			drone.get_position() #get current position
			current_time = rospy.get_time() #get current time
			dt = current_time - old_time #compute time since last command update
			old_time = current_time #update old time
			#compute control variables
			if (drone.start_control):
				[phi_c, theta_c, climb_rate, yaw_rate, err_x, err_y, err_z, err_yaw] = p.calculateCommanded(drone.des_pos, drone.actual_pos, dt)
				drone.set_vel(phi_c, theta_c, climb_rate, yaw_rate) #publish to /cmd_vel
				drone.pub_error(err_x,err_y,err_z, err_yaw) #publish error to /error
				output_list = drone.output() #store variables of interest. NOT USED
			# print(output_list)
			rate.sleep()
	
	except KeyboardInterrupt:
		# kill switch, land the drone on interrupt
		drone.set_vel(0,0,0,0)
		drone.land()
		rospy.spin() 
	rospy.spin()


	
	
