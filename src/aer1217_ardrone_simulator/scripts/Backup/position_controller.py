#!/usr/bin/env python2

"""Class for writing position controller."""

from __future__ import division, print_function, absolute_import

# Import ROS libraries
import roslib
import rospy
import math
import numpy as np

# Import class that computes the desired positions
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import TransformStamped, Twist

def wrap(angle):
	if (angle == 2*math.pi):
		return 0
	elif (angle == -2*math.pi):
		return 0
	if (angle > math.pi):
		return (angle%math.pi) - math.pi
	elif (angle < -math.pi):
		return (angle%math.pi)
	else:
		return angle

class PositionController(object):
	"""ROS interface for controlling the Parrot ARDrone in the Vicon Lab."""
	# write code here for position controller
	def __init__(self, kp, ki, kd):
		self.kp = kp; #Proportional Gain
		self.ki = ki; #Integral Gain
		self.kd = kd; #Derivative Gain
		self.e_prev = np.zeros((1,3)); #Previous error
		self.e_total = np.zeros((1,4)); #Total error
		self.z_dot_old = 0; #Previous climb rate
		self.e_curr = np.zeros((1,3)) #Current error
		return

	def calculateCommanded(self, desiredPos, currentPos, dt):
	#compute control variables
		g = 9.81; #gravity

		#Current error
		self.e_curr = desiredPos - currentPos;
		self.e_curr[0,3] = wrap(self.e_curr[0,3])
		yaw = currentPos[0,3]

		#Total error
		self.e_total += self.e_curr*dt 

		#Accelerations in x y z
		#Generic eqn: r_ddot = kp*e + kd*e_dot

		x_doubledot = self.kp[0]*(self.e_curr[0,0]) + self.kd[0]*( (self.e_curr[0,0]-self.e_prev[0,0])/dt ) + self.ki[0]*(self.e_total[0,0]);
		y_doubledot = self.kp[1]*(self.e_curr[0,1]) + self.kd[1]*( (self.e_curr[0,1]-self.e_prev[0,1])/dt ) + self.ki[1]*(self.e_total[0,1]);

		x_ddot = x_doubledot*math.cos(yaw) + y_doubledot*math.sin(yaw)
		y_ddot = -x_doubledot*math.sin(yaw) + y_doubledot*math.cos(yaw)

		z_doubledot = 0 #self.kp[2]*(self.e_curr[0,2]) + self.kd[2]*( (self.e_curr[0,2]-self.e_prev[0,2])/dt );


		#Commanded angles from derivation
		z_g = z_doubledot+g;

		#theta_c = math.atan(x_ddot/z_g)
		#phi_c = math.atan(-y_ddot/(math.sqrt(x_ddot*x_ddot + z_g*z_g)))
		
		# theta_c = math.atan(x_ddot/g)
		# phi_c = math.atan(-y_ddot*math.cos(theta_c)/g)
		
		theta_c = math.atan(((x_doubledot + y_doubledot*math.tan(yaw))/(z_g))*(1/(math.cos(yaw) + math.tan(yaw)*math.sin(yaw)))); #pitch control
		phi_c = math.atan((math.cos(theta_c)/math.cos(yaw))*(math.tan(theta_c)*math.sin(yaw) - y_doubledot/z_g)); #roll control 
		
		#Climb rate v = vi + a*t
		z_dot = self.kp[2]*(self.e_curr[0,2]); #climb rate from 
		psi_c_dot = self.kp[3]*(self.e_curr[0,3]); #yaw rate

		#Putting control parameters out together
		controlMagic = [phi_c, theta_c, z_dot, psi_c_dot, self.e_curr[0,0],self.e_curr[0,1],self.e_curr[0,2], self.e_curr[0,3]];

		self.e_prev = self.e_curr; #update previous error with current error
		self.z_dot_old = z_dot; #update old climb rate with new climb rate 
	
		return controlMagic




