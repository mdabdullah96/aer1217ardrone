#import the necessary packages
import cv2
import numpy as np
import glob
import os

#Hazard arrow stuff
HSVLOW = np.array([0,161,100])
HSVHIGH = np.array([179,255,255])

erode = 2
dilate = 0


def hazardDetect(proc_name, image):
	#Masking the image
	hsv_img = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

	mask = cv2.inRange(hsv_img, HSVLOW, HSVHIGH)
	mask = cv2.erode(mask, None, iterations=erode)
	mask = cv2.dilate(mask, None, iterations=dilate)
	output = cv2.bitwise_and(image, image, mask = mask)

	#Converting masked image to bunary (black and white)
	output_bgr = cv2.cvtColor(output, cv2.COLOR_HSV2BGR)
	imgray = cv2.cvtColor(output_bgr, cv2.COLOR_BGR2GRAY)
	ret,thresh = cv2.threshold(imgray, 127, 255, cv2.THRESH_TRUNC)

	#Detection Algo
	#Summing all columns and removing all 0s
	colmsum = [ np.sum(thresh[:,i], axis=None) for i in range(len(thresh[0])) ]
	colmsum = [x for x in colmsum if x > 0]

	#Main detection runs only if the binary image isnt empty (i.e no masked arrow)
	if len(colmsum) > 0:
		#Finding max (i.e the head of the arrow)
		max = np.argmax(colmsum, axis=None)

		#Basically checks which side of the tail the arrow head is on
		if max > (len(colmsum)/2):
			# print('Right')
			cv2.imwrite(proc_name, image)
			return (2, True)

		elif max < (len(colmsum)/2):
			# print('Left')
			cv2.imwrite(proc_name, image)
			return (1, True)
	else:
		# print('Nothing found')
		return (1, False)
