import glob
import cv2 as cv
#import cv2.cv as cvs
import numpy as np
import os
# from tf.transformations

def hoopDetect(path, image, HSV_boundaries):

	#Filter by color first to improve circle detection
	image1 = cv.cvtColor(image, cv.COLOR_BGR2HSV)

	for (lower, upper) in HSV_boundaries:
		lower = np.array(lower, dtype ="uint8")
		upper = np.array(upper, dtype ="uint8")
		mask = cv.inRange(image1, lower, upper)
		mask = cv.dilate(mask, None, iterations=2)
		output = cv.bitwise_and(image, image1, mask = mask)
	output_bgr = cv.cvtColor(output, cv.COLOR_HSV2BGR)

	#Hough Gradient circle detection. 
	gray_img = cv.cvtColor(output_bgr, cv.COLOR_BGR2GRAY)
	detected_hoop = cv.HoughCircles(gray_img, 3, 5, 400, param1=100, param2=100, minRadius=80, maxRadius=170)
	
	D_frame = np.zeros((3,1))
	use_data = False

	if detected_hoop is not None:
		use_data = True

		for circle in detected_hoop[0]:
			center = (int(circle[0]), int(circle[1]))
			radius = int(circle[2])
			cv.circle(image, center, radius, (255,0,0),2)
			cv.imwrite(path, image)

		if len(detected_hoop[0])<2:
			#CAMERA TO DRONE COORDINATES ROTATION MATRIX
			R_c2d = np.mat([[0,0,1], [-1,0,0],[0,-1,0]])

			#INTRINSIC MATRIX OF FRONT CAM
			intrinsic_matrix = np.mat([[568.12,0,315.55], [0,564.82,167.49],[0,0,1]])

			#CONVERTING CENTER OF CIRCLE TO CAMERA FRAME
			inv_int = np.linalg.inv(intrinsic_matrix)
			#Center of circle in 
			center = np.mat([[detected_hoop[0][0][0]], [detected_hoop[0][0][1]], [1]])
			radius = int(detected_hoop[0][0][2])

			distance_C = (734.30239*0.86/(2*radius)/(np.tan(92*np.pi/(180*2))))

			#print(radius_string)
			C_frame = inv_int*center*distance_C
			D_frame = R_c2d*C_frame
			#print(D_frame)

	return (D_frame, use_data)


##----------------- MAIN FUNCTION BODY------------------------##

#Making directory to save processed images	
# if not os.path.exists("processed_hoop_pics"):
# 	os.mkdir("processed_hoop_pics")

# #READING THE IMAGES AND FILE NAMES
# images = [cv.imread(file) for file in glob.glob("hoop_pics/*.jpeg")]
# names = [file for file in glob.glob("hoop_pics/*.jpeg")]
# names = ["processed_" + name for name in names]

# HSV_low = [80,10,120]
# HSV_high = [120,60,150] 
# boundaries = [(HSV_low, HSV_high)]

#DETECTING EACH PIC 1 BY 1
# [hoopDetect(path, image, boundaries) for path, image in zip(names, images)]
