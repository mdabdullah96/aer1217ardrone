#!/usr/bin/env python2

"""ROS Node for publishing desired positions."""

from __future__ import division, print_function, absolute_import

# Import ROS libraries
import roslib
import rospy
import numpy as np
import math
from geometry_msgs.msg import Point, PoseStamped
from tf.transformations import quaternion_from_euler
from std_msgs.msg import String


class ROSDesiredPositionGenerator(object):
	"""ROS interface for publishing desired positions."""
	# write code here for desired position trajectory generator
	def __init__(self):
	#-----------------------Constants-----------------------#
		self.start = np.zeros((3,1)) #start point
		self.end = np.zeros((3,1)) #end point
		self.center = np.zeros((2,1)) #center point
		self.radius = 0 #radius
		self.time = 0 #time for forward pass
		self.traj_type = '' #trajectory type
		self.point = np.zeros((3,1)) #current point
		self.vel = np.zeros((3,1)) #velocity
		self.forward = True #forward pass
		self.dist = 0 #distance from start to end
		self.w = 0 # angular velocity
		self.t_run = 0 #continuous time
		self.yaw_angle = 0 #yaw_angle
	#-----------------------Subscriber/Publisher-----------------------#
		self.pub = rospy.Publisher("/des_pos", PoseStamped, queue_size = 10) #publisher 
		rospy.Subscriber("/desired_trajectory", String, self.user_info) #subscriber 
		return
		
	def pub_msg(self,pose):
		msg = PoseStamped()
		msg.header.stamp = rospy.Time.now() #time stamp
		msg.header.frame_id = 'map'
		msg.pose.position.x = pose[0] #desired x
		msg.pose.position.y = pose[1] #desired y
		msg.pose.position.z = pose[2] #desired z
		quat = quaternion_from_euler(0,0,pose[3]) #desired yaw angle as quaternion
		msg.pose.orientation.x = quat[0]
		msg.pose.orientation.y = quat[1]
		msg.pose.orientation.z = quat[2]
		msg.pose.orientation.w = quat[3]
		self.pub.publish(msg)
		return

	#Obtain trajectory information
	def user_info(self,msg):
		self.traj_type = msg.data

		if self.traj_type == 'linear':
			#start_str = raw_input('Enter start point (x,y,z): ') #start point
			#end_str = raw_input('Enter end point (x,y,z): ') #end point
			self.time = 7.5 #time of trajectory in forward pass

			self.start = np.array([-1,2,1]) #start point
			self.point = np.copy(self.start) 
			self.end = np.array([1,0,2]) #end point
			diff = self.end - self.start
			self.vel = diff/self.time #time based velocity
			self.dist = np.linalg.norm(diff) #Eucledian distance

		elif self.traj_type == 'circular':
			#center_str = raw_input('Enter circle center (x,y): ') #center point
			self.time = 15 #time to complete 1 circle
			self.radius = 1 #radius
			self.sprials = 0 #no. of spirals
			self.center = np.array([0,1,1.5]) #center location
			self.w = 2*np.pi/self.time # angular velocity
			#self.vel = (1.5-0.5)/(self.time*self.sprials) #velocity in z
			self.vel = 1 #velocity in z =0 for closed circle
		else:
			print('Please enter allowable tracjectory type!')
		return

	#compute next point in trajectory
	def compute_traj(self,dt):
		#for linear trajectories
		if self.traj_type == 'linear':
			if self.forward: #going forward
				self.point = self.point + self.vel*dt #xk = xk-1 + v*dt
				if np.linalg.norm(self.point - self.start) >= self.dist: #until we reach end point
					self.forward = False #time to go backwards
					self.point = np.copy(self.end)
			else: #reverse direction
				self.point = self.point - self.vel*dt #xk = xk-1 - v*dt
				if np.linalg.norm(self.point - self.end) >= self.dist:
					self.forward = True #time to go forward
					self.point = np.copy(self.start)
			#set yaw to point at endpoint as no mention of value in lab manual		
			self.yaw_angle =  math.atan2(self.end[1] - self.start[1],self.end[0] - self.start[0])

		#--------------------------------------------------------------------------#
		else: #circular trajectories
			if self.forward:
				x = self.center[0] + self.radius*np.cos(self.w*self.t_run) #xk = xc + r*cos(w*k*dt)
				y = self.center[1] + self.radius*np.sin(self.w*self.t_run) #yk = yc + r*sin(w*k*dt)
				z = self.center[2] + self.vel*dt				
				if self.t_run % self.time == 0 and self.t_run != 0: #end point reached
					#x = self.center[0] + self.radius*np.cos(self.w*self.t_run)
					#y = self.center[1] + self.radius*np.sin(self.w*self.t_run)
					#z = 1.5
					self.forward = False #start going backwards
			else:
				x = self.center[0] + self.radius*np.cos(self.w*self.t_run)
				y = self.center[1] + self.radius*np.sin(self.w*self.t_run)
				z = self.center[2] - self.vel*dt
				if self.t_run % self.time == 0:
					#x = self.center[0] + self.radius*np.cos(-self.w*self.t_run)
					#y = self.center[1] + self.radius*np.sin(-self.w*self.t_run)
					#z = 0.5
					self.forward = True

			# x = self.center[0] + self.radius*np.cos(self.w*self.t_run) #xk = xc + r*cos(w*k*dt)
			# y = self.center[1] + self.radius*np.sin(self.w*self.t_run) #yk = yc + r*sin(w*k*dt)
			z = self.center[2] + self.vel*dt #zk = zc + v*dt here v=0
			
			self.point = np.array([x,y,z])
			#set yaw to point at center
			self.yaw_angle =  math.atan2(-(self.point[1] - self.center[1]),-(self.point[0] - self.center[0]))
			self.t_run += dt
		return np.append(self.point, self.yaw_angle)

if __name__ == '__main__':
	rospy.init_node('desired_position')
	r = ROSDesiredPositionGenerator() #create instance
	#r.user_info()
	freq = 30 # Hz
	rate = rospy.Rate(freq)
	try:
		# publish the trajectory 
		while not rospy.is_shutdown():
			if r.traj_type != '': #wait for keypress
				point = r.compute_traj(1/freq) #compute point
				r.pub_msg(point) #publish point
				rate.sleep()
	except KeyboardInterrupt:
		# kill switch, land the drone on interrupt
		data_pnt = Point(0,0,0)
		r.pub_msg(data_pnt)
	rospy.spin()
